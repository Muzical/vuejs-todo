// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from "vue";

//Vue.config.productionTip = false

import VueRouter from "vue-router";

Vue.use(VueRouter);

import Todo from "./pages/Todo";
import Item from "./pages/Item";

const router = new VueRouter({
  routes: [
    { path: "/", component: Todo },
    { path: "/list/:listID/item/:itemID", component: Item, props: true }
  ]
});

import Vuex from "vuex";

Vue.use(Vuex);

import TodoStore from './Store'

const store = new Vuex.Store( TodoStore );

import App from './App'

/* eslint-disable no-new */
new Vue({
  el: "#app",
  store,
  router,
  render: h => h(App)
});
