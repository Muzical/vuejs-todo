# todo

> A Vue.js project based on the tutorial at https://code.tutsplus.com/courses/get-started-with-vue

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

## NOTES
This app still acts in ways for which I do not care, but I am as yet too inexperienced to figure out precisely how to fix this.  I have compared it to the finished code files on GitHub and the "official" code has these same behavorial quirks.
The coding style used is different than my own.  I left it as thus to demonstrate that I will use whatever coding style I am told to use, regardless of my own preference.
